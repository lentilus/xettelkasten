#!/usr/bin/env bash

info() {
	# tmux display-message -d 500 "$1" 
    notify-send "xettelkasten" "$1"
    log "info: $1"
}

editor_edit() {
    "$NVIM_CMD" --server "$NVIM_PIPE" --remote "$1"
}

nvim_cmd() {
    exit
}

nvim_expr() {
    if [[ -z $1 ]]; then
        log "empty expression: $1"
        exit
    fi
    "$NVIM_CMD" --server "$NVIM_PIPE" --remote-expr "$1"
}

editor_current() (
    nvim_expr "expand('%')" &> /tmp/zettelkasten_open
    open=$(</tmp/zettelkasten_open)

    if [[ -z $open ]]; then
        log "no open file found"
        exit
    fi
    cd "$ZETTEL_DATA" || exit
    current="$(basename "$(dirname "$open")")"
    log "editor file: $current"
    echo "$current"
)

viewing() {
    editor_current
}

zettel_session() {
    if ! tmux has-session -t="$SESSION_NAME" 2> /dev/null; then
		log "creating new session"
		rm -rf "$NVIM_PIPE"

        cmd="$NVIM_CMD --listen $NVIM_PIPE"
		tmux new-session -c "$ZETTEL_DATA" -d -s "$SESSION_NAME" "$cmd" || log "session creation failed"
        sleep 0.05
    fi

	log "tmux switch client"
    tmux switch-client -t "$SESSION_NAME"
}

user_input() {
    nvim_expr "input('$1', '$2', 'file')" &> /tmp/zettelkasten_input || log "error with nvim_expr"
    open=$(</tmp/zettelkasten_input)
    log "user input is $open"

    [[ -z $open ]] && exit 
    echo "$open"
}
